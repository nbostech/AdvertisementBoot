package com.wavelabs.test.dao;

import static org.mockito.Mockito.when;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.dao.AdvertisementDao;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;
import com.wavelabs.modal.enums.AdvertisementType;
import com.wavelabs.utility.Helper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Helper.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdvertisementDaoTest {

private Advertisement add;
	private SessionFactory factory;
	private User user;

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() {

		Configuration cfg = new Configuration();
		factory = cfg.configure("hibernateTest.cfg.xml").buildSessionFactory();
		createUser();
		createAdvertisement();
		PowerMockito.mockStatic(Helper.class);
		Session session = factory.openSession();
		when(Helper.getSession()).thenReturn(session);
	}

	public void createUser() {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		user = new User();
		user.setId(1);
		user.setName("gopi krishna");
		user.setEmail("gopi@gmail.com");
		session.save(user);
		tx.commit();
		session.close();
	}

	public void createAdvertisement() {
		add = new Advertisement(1, "Sell phone", AdvertisementType.Education, "Selling of phone for 10k", user,
				"Khammam");
	}

	@Test
	public void testPersistAdvertisement() {

		boolean flag = AdvertisementDao.persistAdvertisement(add);
		Assert.assertEquals(true, flag);

	}

	@Test
	public void testUpdatePersistAdvertisement() {
		add.setLocation("Vatsavai");

		Advertisement add2 = AdvertisementDao.updateAdvertisement(add);
		Assert.assertEquals("Vatsavai", add2.getLocation());
	}

	@Test
	public void testGetAdvertisement() {

		Advertisement add3 = AdvertisementDao.getAdvertisement(1);
		Assert.assertEquals("Sell phone", add3.getName());
	}

	@Test
	public void testGetListOfAdvertisements() {
		List<Advertisement> list = AdvertisementDao.getAllAdvertisementsOfUser(2);
		Assert.assertEquals(1, list.size());
	}

	@Test
	public void testDeleteAdvertisement() {
		boolean flag = AdvertisementDao.deleteAdvertisement(1);
		Assert.assertEquals(true, flag);
	}
}
