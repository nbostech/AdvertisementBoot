package com.wavelabs.test.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.dao.AdvertisementDao;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;
import com.wavelabs.multithread.ConfirmationThread;
import com.wavelabs.service.AdvertisementService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AdvertisementDao.class, ConfirmationThread.class })
public class TestAdvertisementService {

	private Advertisement add;

	@Before
	public void setUp() {

		add = new Advertisement();
		add.setId(1);
		add.setDescription("This is test add");
		add.setLocation("Hyderabad");
		add.setName("Phone Add");
		User user = new User();
		user.setId(1);
		user.setName("Gopi krishna");
		user.setEmail("gopi@gmail.com");
		add.setUser(user);
		System.out.println("Hey");
	}

	/*@Test
	public void testPersistAdvertisement() throws Exception {
		Advertisement add2 = mock(Advertisement.class);
		PowerMockito.mockStatic(AdvertisementDao.class);
		
		 * PowerMockito.spy(Thread.class);
		 * PowerMockito.doNothing().when(Thread.class);
		 
		when(AdvertisementDao.persistAdvertisement(any(Advertisement.class))).thenReturn(true);
		boolean flag = AdvertisementService.persistAdvertisement(add2);
		Assert.assertEquals(true, flag);
	}*/

	@Test
	public void testGetAdvertisement() {

		PowerMockito.mockStatic(AdvertisementDao.class);
		when(AdvertisementDao.getAdvertisement(any(Integer.class))).thenReturn(add);
		Advertisement ad2 = AdvertisementService.getAdvertisement(1);
		Assert.assertEquals(ad2.getLocation(), "Hyderabad");

	}

/*	@Test
	public void testUpdateAdvertisement() {

		add.setName("Nutana");
		PowerMockito.mockStatic(AdvertisementDao.class);
		when(AdvertisementDao.updateAdvertisement(add)).thenReturn(add);
		Advertisement newAdd = AdvertisementService.updateAdvertisement(add);
		Assert.assertEquals("Phone add", newAdd.getName());

	}*/

	@Test
	public void testDeleteAdvertisement() {
		PowerMockito.mockStatic(AdvertisementDao.class);
		when(AdvertisementDao.deleteAdvertisement(any(Integer.class))).thenReturn(true);
		boolean flag = AdvertisementService.deleteAdvertisement(2);
		Assert.assertEquals(true, flag);
		PowerMockito.verifyStatic();
	}
}
