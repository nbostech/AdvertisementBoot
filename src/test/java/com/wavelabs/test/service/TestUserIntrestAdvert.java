package com.wavelabs.test.service;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import com.wavelabs.dao.UserAdvertIntrestedDao;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;
import com.wavelabs.modal.UserAdvertIntrested;
import com.wavelabs.modal.enums.AdvertisementType;
import com.wavelabs.service.UserAdvertIntrestService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserAdvertIntrestedDao.class)
public class TestUserIntrestAdvert {

	@Test
	public void testPersistUserIntrestAdvert() {

		PowerMockito.mockStatic(UserAdvertIntrestedDao.class);
		UserAdvertIntrested uai = mock(UserAdvertIntrested.class);
		when(UserAdvertIntrestedDao.persistUserAdverIntrested(uai)).thenReturn(true);
		boolean flag = UserAdvertIntrestService.persistUserAdvert(uai);
		Assert.assertEquals(true, flag);
	}

	@Test
	public void testGetUserAdvert() {

		PowerMockito.mockStatic(UserAdvertIntrestedDao.class);
		UserAdvertIntrested uai = new UserAdvertIntrested(1,
				new User(1, "gopi krishna", "gopi@gmail.com", "9032118864"),
				new Advertisement(1, "phone", AdvertisementType.Business, "Short desc", null, "Hyd"));
		when(UserAdvertIntrestedDao.get(anyInt())).thenReturn(uai);
		UserAdvertIntrested newUai = UserAdvertIntrestService.getUserAdvert(1);
		Assert.assertEquals("gopi@gmail.com", newUai.getUser().getEmail());
	}

	@Test
	public void testGetAllUserAdverts() {

		PowerMockito.mockStatic(UserAdvertIntrestedDao.class);
		UserAdvertIntrested uai1 = mock(UserAdvertIntrested.class);
		UserAdvertIntrested uai2 = mock(UserAdvertIntrested.class);
		List<UserAdvertIntrested> list = new ArrayList<UserAdvertIntrested>();
		list.add(uai1);
		list.add(uai2);
		when(UserAdvertIntrestService.getAllUserAdvertIntrests(anyInt())).thenReturn(list);
		List<UserAdvertIntrested> newList = UserAdvertIntrestService.getAllUserAdvertIntrests(2);
		Assert.assertEquals(2, newList.size());

	}
}
