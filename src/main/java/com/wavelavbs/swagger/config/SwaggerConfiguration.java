package com.wavelavbs.swagger.config;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import io.swagger.jaxrs.config.BeanConfig;

public class SwaggerConfiguration extends HttpServlet {

	private static final long serialVersionUID = 2817294331539721502L;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setTitle("Search engine for Advertisements");
		beanConfig.setVersion("1.0");
		beanConfig.setSchemes(new String[] { "http" });
		beanConfig.setHost("10.9.9.112:8080");
		beanConfig.setBasePath("/Advertisement/webapi/");
		beanConfig.setResourcePackage("com.wavelabs.resource");
		beanConfig.setScan(true);
		beanConfig.setDescription("Faster full text search engine");
	}
}
