package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wavelavbs.swagger.config.SwaggerConfiguration;

import io.swagger.jaxrs.config.BeanConfig;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Component
@ApplicationPath("/Advertisement/webapi")
public class JerseyConfig extends ResourceConfig {

	@Autowired
	public JerseyConfig(ObjectMapper objectMapper) {
		// register endpoints
		packages("com.wavelabs.resource", "io.swagger.jaxrs.listing");
		// register jackson for json
		register(SwaggerConfiguration.class);
		register(new ObjectMapperContextResolver(objectMapper));
	}

	@PostConstruct
	public void init() {
		// Register components where DI is needed
		this.configureSwagger();
	}

	@Provider
	public static class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {

		private final ObjectMapper mapper;

		public ObjectMapperContextResolver(ObjectMapper mapper) {
			this.mapper = mapper;
		}

		@Override
		public ObjectMapper getContext(Class<?> type) {
			return mapper;
		}
	}

	private void configureSwagger() {

		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setTitle("Search engine for Advertisements");
		beanConfig.setVersion("1.0");
		beanConfig.setSchemes(new String[] { "http" });
		beanConfig.setHost("localhost:8080");
		beanConfig.setBasePath("/Advertisement/webapi/");
		beanConfig.setResourcePackage("com.wavelabs.resource");
		beanConfig.setScan(true);
		beanConfig.setDescription("Faster full text search engine");
	}
}