package com.wavelabs.solr.service;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;

import com.wavelabs.dao.UserDAO;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;
import com.wavelabs.modal.enums.AdvertisementType;

public class SolrSearchService {
	private static SolrClient client = null;

	private static String parseString(String str) {
		return str.replace("[", "").replace("]", "");
	}

	private static void intillization() {
		client = new HttpSolrClient("http://localhost:8983/solr/advert");
	}

	public static Advertisement[] getAdverts(String searchTerm) {
		intillization();
		/* SolrQuery query = new SolrQuery("q=" + searchTerm + "~3"); */
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("spellcheck", "on");
		params.set("q", searchTerm + "~3");
		try {
			QueryResponse response = client.query(params);
			SolrDocumentList documentList = response.getResults();
			Advertisement[] adds = new Advertisement[documentList.size()];
			int i = 0;
			for (SolrDocument document : documentList) {
				Integer id = Integer.parseInt(parseString(document.getFieldValue("id").toString()));
				String name = parseString(document.getFieldValue("name").toString());
				String type = parseString(document.getFieldValue("type").toString());
				String description = parseString(document.getFieldValue("description").toString());
				Integer user_id = Integer.parseInt(parseString(document.getFieldValue("user_id").toString()));
				String location = parseString(document.getFieldValue("location").toString());
				User user = UserDAO.getUser(user_id);
				Advertisement add = createAdvertisement(user, id, name, description, type, location);
				adds[i++] = add;
			}
			return adds;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Advertisement createAdvertisement(User user, Integer id, String name, String description,
			String type, String location) {

		Advertisement add = new Advertisement();
		add.setDescription(description);
		add.setId(id);
		add.setName(name);
		add.setUser(user);
		add.setLocation(location);
		add.setType(AdvertisementType.valueOf(type));
		return add;
	}

	public static void main(String[] args) throws SolrServerException, IOException {
		Advertisement ad[] = getAdverts("Phone");
		for (Advertisement add : ad) {
			System.out.println(add.getName());
		}
	}

}
