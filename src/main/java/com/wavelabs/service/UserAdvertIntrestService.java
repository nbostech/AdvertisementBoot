package com.wavelabs.service;


import java.util.List;
import com.wavelabs.dao.UserAdvertIntrestedDao;
import com.wavelabs.modal.UserAdvertIntrested;
import com.wavelabs.multithread.ConfirmationThread;

public class UserAdvertIntrestService {

	public static boolean persistUserAdvert(UserAdvertIntrested uia) {
		boolean flag = UserAdvertIntrestedDao.persistUserAdverIntrested(uia);
		if (flag) {
			ConfirmationThread confirmationThread = new ConfirmationThread(uia.getAdvert(), uia.getUser(), "APPLY");
			Thread thread = new Thread(confirmationThread);
			thread.start();
		} 
		return flag;
	}

	public static UserAdvertIntrested getUserAdvert(int id) {

		return UserAdvertIntrestedDao.get(id);
	}

	public static List<UserAdvertIntrested> getAllUserAdvertIntrests(int id) {

		return UserAdvertIntrestedDao.getUserAdvertIntrested(id);
	}
}
