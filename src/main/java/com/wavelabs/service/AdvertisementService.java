package com.wavelabs.service;

import java.util.List;
import com.wavelabs.dao.AdvertisementDao;
import com.wavelabs.dao.CommentDAO;
import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.Comment;
import com.wavelabs.multithread.ConfirmationThread;

public class AdvertisementService {

	public static boolean persistAdvertisement(Advertisement add) {

		boolean flag = AdvertisementDao.persistAdvertisement(add);
		if (flag) {
			ConfirmationThread confirmationThread = new ConfirmationThread(add, add.getUser(), "POST");
			Thread thread = new Thread(confirmationThread);
			thread.start();
		}
		return flag;
	}

	public static Advertisement getAdvertisement(int id) {

		return AdvertisementDao.getAdvertisement(id);
	}

	public static Advertisement updateAdvertisement(Advertisement add) {

		return AdvertisementDao.updateAdvertisement(add);
	}

	public static boolean deleteAdvertisement(int id) {
		return AdvertisementDao.deleteAdvertisement(id);
	}

	public static List<Advertisement> getAllAdvertisementOfUser(int id) {

		return AdvertisementDao.getAllAdvertisementsOfUser(id);
	}

	public static Boolean persistComment(Comment comment) {

		return CommentDAO.persistComment(comment);

	}

	public static Comment[] getListOfCommentsOfAdvertisement(int id) {

		return CommentDAO.getAdvertisementComments(id);
	}

	public static Comment updateComment(Comment comment) {

		return CommentDAO.updateComment(comment);
	}

	public static Boolean deleteComment(int id) {
		Boolean flag = CommentDAO.deleteComment(id);
		return flag;
	}
}
