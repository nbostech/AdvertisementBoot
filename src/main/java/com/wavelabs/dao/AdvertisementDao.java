package com.wavelabs.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.wavelabs.modal.Advertisement;
import com.wavelabs.modal.User;
import com.wavelabs.utility.Helper;

public class AdvertisementDao {

	public static Advertisement getAdvertisement(int id) {
		Session session = Helper.getSession();
		Advertisement add = (Advertisement) session.get(Advertisement.class, id);
		session.close();
		return add;
	}

	public static boolean persistAdvertisement(Advertisement add) {
		Session session = Helper.getSession();
		try {
			Transaction tx = session.beginTransaction();
			session.save(add);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public static Advertisement updateAdvertisement(Advertisement add) {
		Session session = Helper.getSession();
		Transaction tx = session.beginTransaction();
		try {
			session.update(add);
			tx.commit();
			return add;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static boolean deleteAdvertisement(int id) {
		Session session = Helper.getSession();
		Transaction tx = session.beginTransaction();
		try { 
			Advertisement add = (Advertisement) session.get(Advertisement.class, id);
			session.delete(add);
			tx.commit();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static List<Advertisement> getAllAdvertisementsOfUser(int id) {

		Session session = Helper.getSession();
		try {
			String hql = " from " + Advertisement.class.getName() + " where user=:user";
			Query query = session.createQuery(hql);
			User user = (User) session.get(User.class, id);
			query.setParameter("user", user);
			List<Advertisement> adds = query.list();
			return adds;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
