package com.wavelabs.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.job.service.SolrJobIndexScheduleService;
import com.wavelabs.modal.Message;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Solr Job")
@Path("/solr")
public class SolrJob {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{time}")
	@ApiOperation(value = "Starts data import to solr from Database with given interval of time")
	@ApiResponses({ @ApiResponse(code = 200, message = "Solr indexing job started"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public Response startSolrJob(@ApiParam(name = "time") @PathParam("time") Integer time) {

		boolean flag = SolrJobIndexScheduleService.startSchedule(time);
		Message message = new Message();
		if (flag) {
			message.setId(200);
			message.setMessage("Solr auto index completed Successfully");
			return Response.status(200).entity(message).build();
		} else {
			message.setId(500);
			message.setMessage("Problem occured");
			return Response.status(200).entity(message).build();
		}
	}

	@GET
	@Path("stop")
	@ApiOperation(value = "Stops the job on solr")
	@ApiResponses({ @ApiResponse(code = 200, message = "Solr indexing stopped"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public Response stopSolrJob() {

		boolean flag = SolrJobIndexScheduleService.stopSchedule();
		Message message = new Message();
		if (flag) {
			message.setId(200);
			message.setMessage("Solr auto index stopped Successfully");
			return Response.status(200).entity(message).build();
		} else {
			message.setId(500);
			message.setMessage("Problem occured");
			return Response.status(500).entity(message).build();
		}
	}
}
